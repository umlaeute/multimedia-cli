#!/usr/bin/env python3

import base64
from debian import deb822
import os
import pprint
import requests
import sys
import urllib.parse

API_URL="https://salsa.debian.org/api/v4/"

class SalsaSession:
    '''Wrapper around requests.Session with some helper methods'''
    def __init__(self):
        self._session = requests.Session()
        with open("TOKEN", 'r') as f:
            self._token = f.read().strip()

    def request(self, method, *args, **kwargs):
        '''
        Runs a request
         args = parts of request, url encoded, then joined with /
         kwargs = passed to requests api
        '''

        # Construct request URL
        url = API_URL + '/'.join((urllib.parse.quote_plus(str(x)) for x in args))

        # Add our private token header
        headers = kwargs.pop("headers", {})
        headers["Private-Token"] = self._token

        # Log all non-get queries
        if method != "get":
            print(method, url, kwargs)

        # Perform the request and check for errors
        r = self._session.request(method, url, headers=headers, **kwargs)
        r.raise_for_status()
        if r.status_code != 204:
            return r.json()

    def get(self, *args, **kwargs):
        return self.request('get', *args, **kwargs)

    def get_unpaginated(self, *args, **kwargs):
        '''Like get, but get the entire unpaginated response'''
        params = kwargs.pop("params", {})
        params["per_page"] = 100
        params["page"] = 1

        result = []
        while True:
            result_page = self.get(*args, params=params, *kwargs)
            if result_page:
                result.extend(result_page)
                params["page"] += 1
            else:
                return result

    def post(self, *args, **kwargs):
        return self.request('post', *args, **kwargs)

    def put(self, *args, **kwargs):
        return self.request('put', *args, **kwargs)

    def delete(self, *args, **kwargs):
        return self.request('delete', *args, **kwargs)

class GroupConfig:
    '''Stores the configuration settings for a group'''
    def __init__(self, path, maintainer, hooks_present, hooks_absent=[]):
        self.path = path
        self.maintainer = maintainer
        self.hooks_present = hooks_present
        self.hooks_absent = hooks_absent

class HookResult:
    '''Hook exists and has correct config'''
    GOOD = 0

    '''Hook exists but has bad config'''
    BAD = 1

    '''Hook does not exist / inactive'''
    NON_EXISTANT = 2

    '''Hook needs source package but none was given'''
    NEEDS_SOURCE = 3

class HookService:
    '''Base class for "service" style hooks'''
    def properties_for_project(self, project, source):
        '''
        Returns dict containing correct properties config for a project
        or NEEDS_SOURCE if source is needed and not given.
        '''
        raise NotImplementedError

    def audit(self, s, project, source):
        live_data = s.get("projects", project["id"], "services", self.name)

        if live_data["active"]:
            good_props = self.properties_for_project(project, source)
            if good_props == HookResult.NEEDS_SOURCE:
                return (HookResult.NEEDS_SOURCE, None)

            # Validate each config entry
            record = []
            if not live_data["push_events"]:
                record.append("push events disabled")
            if not live_data["tag_push_events"]:
                record.append("tag events disabled")

            for key, value in good_props.items():
                if live_data["properties"][key] != value:
                    record.append(key + " is wrong")

            if record:
                return (HookResult.BAD, record)
            else:
                return (HookResult.GOOD, None)
        else:
            return (HookResult.NON_EXISTANT, None)

    def add(self, s, project, source):
        '''
        Adds a service, knowing it does not currently exist
         Returns GOOD or NEEDS_SOURCE
        '''
        props = self.properties_for_project(project, source)
        if props == HookResult.NEEDS_SOURCE:
            return HookResult.NEEDS_SOURCE

        s.put("projects", project["id"], "services", self.name, data=props)
        return HookResult.GOOD

    def delete(self, s, project, source):
        '''Deletes a service'''
        s.delete("projects", project["id"], "services", self.name)
        return HookResult.GOOD

class HookEmail(HookService):
    '''Emails on push service'''
    def __init__(self):
        self.name = "emails-on-push"

    def properties_for_project(self, project, source):
        if source:
            return { "recipients": "dispatch+" + source + "_vcs@tracker.debian.org" }
        else:
            return HookResult.NEEDS_SOURCE

class HookIrker(HookService):
    '''Irker IRC notifications service'''
    def __init__(self, server, irc_uri, channel):
        self.name = "irker"
        self.server = server
        self.irc_uri = irc_uri
        self.channel = channel

    def properties_for_project(self, project, source):
        return  {
            "colorize_messages": True,
            "default_irc_uri": self.irc_uri,
            "recipients": self.channel,
            "server_host": self.server
        }

class HookWebhook:
    '''Base class for "webhook" style hooks'''
    def url_for_project(self, project, source):
        '''
        Returns the URL for this webhook or NEEDS_SOURCE if source is needed
        and not given.
        '''
        raise NotImplementedError

    def find_hook(self, s, project, url):
        '''
        Returns information about a hook with the given url in a project or None
        if no hook exists.
        '''
        for hook in s.get("projects", project["id"], "hooks"):
            if hook["url"] == url:
                return hook

        return None

    def audit(self, s, project, source):
        url = self.url_for_project(project, source)
        if url == HookResult.NEEDS_SOURCE:
            return (HookResult.NEEDS_SOURCE, None)

        hook = self.find_hook(s, project, url)
        if hook:
            record = []
            if not hook["enable_ssl_verification"]:
                record.append("ssl verification disabled")

            # Currently hardcoded to check push events only
            if not hook["push_events"]:
                record.append("push events disabled")

            if record:
                return (HookResult.BAD, record)
            else:
                return (HookResult.GOOD, None)
        else:
            return (HookResult.NON_EXISTANT, None)

    def add(self, s, project, source):
        '''
        Adds a webhook, knowing it does not currently exist
         Returns GOOD or NEEDS_SOURCE
        '''
        url = self.url_for_project(project, source)
        if url == HookResult.NEEDS_SOURCE:
            return HookResult.NEEDS_SOURCE

        data = {
            "url": url,
            "push_events": True,
            "enable_ssl_verification": True
        }

        s.post("projects", project["id"], "hooks", data=data)
        return HookResult.GOOD

    def delete(self, s, project, source):
        '''Deletes a webhook'''
        url = self.url_for_project(project, source)
        if url == HookResult.NEEDS_SOURCE:
            return HookResult.NEEDS_SOURCE

        hook = self.find_hook(s, project, url)
        if hook:
            s.delete("projects", project["id"], "hooks", hook["id"])
        return HookResult.GOOD

class HookTagPending(HookWebhook):
    '''Hook which marks closed bugs as pending'''
    def __init__(self):
        self.name = "bts pending hook"

    def url_for_project(self, project, source):
        if source:
            return "https://webhook.salsa.debian.org/tagpending/" + source
        else:
            return HookResult.NEEDS_SOURCE

def args2group(args):
    hooks = list()
    if True: # args.pending:
        hooks.append(HookTagPending())
    if True: # args.hook_email:
        hooks.append(HookEmail())
    if args.irc:
        hooks.append(HookIrker("ruprecht.snow-crash.org", "irc://irc.oftc.net:6667/", args.irc))
    return GroupConfig(args.path, args.maintainer, hooks)

MULTIMEDIA_TEAM = GroupConfig(
        "multimedia-team",
        "Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>",
        [HookTagPending(),
         HookEmail(),
         HookIrker("ruprecht.snow-crash.org", "irc://irc.oftc.net:6667/", "#debian-multimedia")]
    )


#def task_audit(group, fix=False):
def task_audit(args):
    group = args2group(args)
    fix = args.fix
    def print_colour(colour, text):
        if os.isatty(0):
            print(colour, end='')
            print(text, end='')
            print('\033[0m')
        else:
            print(text)

    s = SalsaSession()
    for project in s.get_unpaginated("groups", group.path, "projects",
            params={"order_by": "path", "sort": "asc"}):
        record = []
        deb_control_source = None

        print(project['path'] + ': ', end='')

        # Ignore projects which are shared with us
        if project["namespace"]["full_path"] != group.path:
            print_colour('\033[93m', "IGNORE")
            continue

        # Basics
        if project["visibility"] != "public":
            record.append("not public")
            if fix:
                s.post("projects", project["id"], data={"visibility": "public"})
                record[-1] += " (fixed)"

        if project["issues_enabled"]:
            record.append("issues enabled")
            if fix:
                s.post("projects", project["id"], data={"issues_enabled": "0"})
                record[-1] += " (fixed)"

        # debian/control
        control_file = None
        try:
            control_file = s.get(
                    "projects",
                    project["id"],
                    "repository",
                    "files",
                    "debian/control",
                    params={"ref": project["default_branch"]})
        except requests.exceptions.HTTPError:
            pass

        if control_file:
            control_data = None
            try:
                control_data = base64.b64decode(control_file["content"]).decode("utf-8")
            except UnicodeError:
                record.append("debian/control: invalid utf-8")

            if control_data:
                control_dict = deb822.Deb822(control_data)
                if "Source" in control_dict:
                    deb_control_source = control_dict["Source"]
                    if deb_control_source != project["path"]:
                        record.append("debian/control: Source does not patch project name")
                    if control_dict.get("Vcs-Git", "") != project["http_url_to_repo"]:
                        record.append("debian/control: wrong Vcs-Git: %s != %s" %
                                      (control_dict.get("Vcs-Git", ""), project["http_url_to_repo"]))
                    if control_dict.get("Vcs-Browser", "") != project["web_url"]:
                        record.append("debian/control: wrong Vcs-Browser: %s != %s" %
                                      (control_dict.get("Vcs-Browser", ""), project["web_url"]))
                    if control_dict.get("Maintainer", "") != group.maintainer:
                        record.append("debian/control: wrong Maintainer: '%s' != '%s'" %
                                      (control_dict.get("Maintainer", ""), group.maintainer))
                else:
                    record.append("debian/control: no Source field")
        else:
            record.append("debian/control: does not exist")

        # Present hooks
        for hook in group.hooks_present:
            (result, record_extra) = hook.audit(s, project, deb_control_source)
            if result in (HookResult.BAD, HookResult.NON_EXISTANT):
                if result == HookResult.NON_EXISTANT:
                    record_extra = ["disabled"]

                if fix:
                    if result == HookResult.BAD:
                        hook.delete(s, project, deb_control_source)
                    hook.add(s, project, deb_control_source)
                    record_extra = (x + " (fixed)" for x in record_extra)

                record.extend((hook.name + ": " + x for x in record_extra))

        # Absent hooks
        for hook in group.hooks_absent:
            (result, _) = hook.audit(s, project, deb_control_source)
            if result in (HookResult.GOOD, HookResult.BAD):
                record.append(hook.name + ": enabled, but should not be")
                if fix:
                    hook.delete(s, project, deb_control_source)
                    record[-1] += " (fixed)"

        # Report issues
        if record:
            print_colour('\033[91m', "FAIL")
            for issue in record:
                print(" ", issue)
        else:
            print("ok")

def task_get(args):
    parts = args.url
    pprint.pprint(SalsaSession().get(*parts))

def task_import(args):
    group = args2group(args)
    project = args.package
    import_url = args.url
    s = SalsaSession()

    # Create project
    data = {
            "path": project,
            "namespace_id": s.get("namespaces", group.path)["id"],
            "description": project + " packaging",
            "issues_enabled": "no",
            "visibility": "public",
            "import_url": import_url
        }
    project_data = s.post("projects", data=data)

    # Setup present hooks
    for hook in group.hooks_present:
        hook.add(s, project_data, project)

    # Print project data
    pprint.pprint(project_data)

def parse_args():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--token",
        type=str, default="TOKEN",
        help="File to read auth token from")
    parser.add_argument("-m", "--maintainer",
        type=str, default=MULTIMEDIA_TEAM.maintainer,
        help="set alternative maintainer address")
    parser.add_argument("-i", "--irc",
        type=str, default="#debian-multimedia",
        help="set alternative IRC-channel")
    parser.add_argument("-p", "--path",
        type=str, default=MULTIMEDIA_TEAM.path,
        help="set alternative salsa path")

    subparsers = parser.add_subparsers(help='sub-command help')

    parser_audit = subparsers.add_parser(
        'audit',
        help='team repositories audit')
    parser_audit.set_defaults(_fun=task_audit)
    parser_audit.add_argument("-f", "--fix",
                              action='store_true', default=False,
                              help="fix repository setup if needed")

    parser_get = subparsers.add_parser(
        'get',
        help='get arbitrary salsa url')
    parser_get.set_defaults(_fun=task_get)
    parser_get.add_argument(
        'url',
        nargs='+', type=str,
        help="path to get")

    parser_import = subparsers.add_parser(
        'import',
        help='import package from URL')
    parser_import.set_defaults(_fun=task_import)
    parser_import.add_argument(
        'package',
        type=str,
        help="package to import")
    parser_import.add_argument(
        'url',
        type=str,
        help="URL to import package from")

    args = parser.parse_args()
    try:
        fun = args._fun
        del args._fun
    except AttributeError:
        parser.print_usage()

        sys.exit(1)
    return (fun, args)

if __name__ == '__main__':
    fun, args = parse_args()
    fun(args)
